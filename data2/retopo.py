from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt

def retopo(name):
    # with fits.open(name) as hdu:
        # if type(hdu[0].data) is np.ndarray:
        #     tmp = hdu[0].data[:,0]
        #     tmp = tmp[0]
        #     tmp = np.where(np.isnan(tmp),0,tmp) 
        # else:
    hdu = fits.open(name)
    for i in range(18):
        hdu.pop()
    hdu.writeto("run12_not_regrided.fits")
    # with fits.open('250mic.fits') as hdu:
    #     hdu[0].data = tmp
    #     hdu.writeto(name + ".r")

def conversionJy19beam(name):
    sec = 4.84814e-6
    cc = 3e8
    with fits.open(name) as hdu:
        nu = cc/(850e-6)*1e-9
        beam = 2460/nu*sec
        omega = 1.133*(19*sec)**2        
        hdu[0].data = hdu[0].data*1/omega*1e-6
        hdu.writeto('l1521e_850mic.fits')
conversionJy19beam('l1521e_850micJy.fits')
# retopo('run12.fits')

        
