* Python dependencies

numpy
matplotlib
astropy
scipy
reproject
emcee 
corner


* usage 

The code needs an Input file describing the parameters to be fitted.
An example of such an input file is nonext_freeall.inp.

to execute (must be in the folder with the data):

python ../src/cont_fit_mcmc_abso.py nonext_freeall.inp outputname [options]

To get help on the options do:

python ../src/cont_fit_mcmc_abso.py -h

with the options you can control the fitting parameters, number of
MCMC steps, number of walkers etc.

In case of doubts let me know.

* usage <2019-07-28 dim.>
I have done some modifications to the code:
** It is now compatible with python3 (except the run corner function)
** Now you have to specify where the obs file are located with the --obs option, there is now two input file 
** You should also put the coordinate of the peak in this file
** There is now a burn-in option to remove part of the first step, aimed to keep only walkers that are in the high probability region.
** There is now a graph-only option to just draw the hiostogram without doing the whole MCMC computation again

Here are some example of command line to launch the simulation:
#+BEGIN_SRC bash
python3 cont_fit_mcmc_abso.py -n 24 -s 200 -b 100 --obs obs_l1498 nonext_freeall.inp out 
#+END_SRC
will launch the simulation with 24 walkers, 200 steps, remove the
first 100 steps, read the file obs and the center of the peak from
obs_l1498 and the guess for the mcmc from nonext_freeall.inp, the
output is named out

#+BEGIN_SRC bash
python3 cont_fit_mcmc_abso.py -n 24 -s 1000 -b 100 --obs obs_l1521e nonext_freeall.inp out -g True
#+END_SRC
will not launch a simulation but take the results simulation named out
to draw histograms.

About the file 'obs' (see obs_l1498 for example):
It has to respect this convention:
path wavelength[m] beam noise size_of_the_cut
x_peak, y_peak


and no '\n' at the end !

