import my_library as ml
import mcmc_utils as mcu
import time
import argparse
import sys
import emcee
from datetime import datetime
from multiprocessing import cpu_count as ncpu
import numpy as np
import os

cc = 3e8

nthreads = ncpu()

parser = argparse.ArgumentParser(description="MCMC tool to fit the continuum")
parser.add_argument("input", type=str, help="initial guesses, alpha, plateau(''), density ratio")
parser.add_argument("outputname", type=str, help="output fits file containing the markov chain")
parser.add_argument("-n","--nwalkers", type=int, default = 30, help="Number of walkers")
parser.add_argument("-s","--steps", type=int, default = 100, help="Number of MCMC iterations")
parser.add_argument("-i","--initialize", type = str, default = None, help = "Start Markov chain from previous iteration")
parser.add_argument("-p","--positionangle", type = float, default = 0.0, help = "position angle of the aperture ellipsis")
parser.add_argument("-r","--aspectratio", type = float, default = 1.0, help = "aspect ratio of the aperture ellipsis")
parser.add_argument("-c","--cut", default = False, action="store_true", help = "aspect ratio of the aperture ellipsis")
parser.add_argument("-b", "--burnin", default = 10, help="Removes first steps to keep only the high probability region")
parser.add_argument("-g", "--graphonly", default = False, help="Just draw the histograms without computing the mcmc")
parser.add_argument('-o', '--obs', default='obs', help="File with names of your .fits and parameters")

args = parser.parse_args()

inpfile = args.input
nwalkers = args.nwalkers
nit = args.steps
itheta,priorinf,priorsup,ndim,fixed,fix,log = mcu.read_input_sel(inpfile)
legends = mcu.read_legend(inpfile)

if args.graphonly:
    answer = mcu.bestfit(args.outputname, legends, log[~fix], burnin=int(args.burnin), save=True, plot=True,fontsize=10)
    exit()

obs = []
cuts = []

with open(args.obs, "r") as obs_file :
    file = obs_file.read()
    lines = file.split("\n")
for line  in lines[0:-2]:
    path, wl, beam, noise, cut = line.split(' ')
    cuts.append(int(cut))
    obs.append(ml.fits_obs(path, beam=float(beam), freq=cc/float(wl), noise=float(noise)))
    print(path, wl, beam, noise)
    
x,y = map(float, lines[-1].split(' '))
print(cuts)
print(x,y)

for i in range(len(obs)):
    #        Center in RA DEC (degrees) "direction of cut" Maximal radius
    obs[i].cut_profile([x,y], (-1,-2), cuts[i])

initial = args.initialize

if initial != None:
    pos, prevchain = mcu.get_pos_and_chain(initial)
else:
    gauss = 1e-3  # we choose to initialize the values in a gaussian ball, this is its width.
    pos = [itheta + gauss * np.random.rand(ndim) for i in range(nwalkers)]  # initial positions for each walker
    prevchain = None

#start = time.time()

sampler = emcee.EnsembleSampler(nwalkers, ndim, ml.lnprob_abso,args=(priorinf,priorsup,fixed,fix,log,obs),threads=nthreads)

mcu.run_sampler(sampler,pos,nit,prevchain,args.outputname)

print("\n\nPlotting walkers\n")

mcu.plot_walkers(args.outputname, legends)

print("Making corner plot\n")

# mcu.run_corner(args.outputname,[nit/2],legends)

answer = mcu.bestfit(args.outputname, legends, log[~fix], burnin=int(args.burnin), save=True, plot=True,fontsize=10)

#mcu.took(start)
